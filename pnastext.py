#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
COPYRIGHT © 2015 
MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES
ALL RIGHTS RESERVED
 
PERMISSION IS GRANTED TO USE, COPY, CREATE DERIVATIVE WORKS AND REDISTRIBUTE
THIS SOFTWARE AND SUCH DERIVATIVE WORKS FOR ANY PURPOSE, SO LONG AS THE NAME
OF MICHIGAN STATE UNIVERSITY IS NOT USED IN ANY ADVERTISING OR PUBLICITY
PERTAINING TO THE USE OR DISTRIBUTION OF THIS SOFTWARE WITHOUT SPECIFIC,
WRITTEN PRIOR AUTHORIZATION.  IF THE ABOVE COPYRIGHT NOTICE OR ANY OTHER
IDENTIFICATION OF MICHIGAN STATE UNIVERSITY IS INCLUDED IN ANY COPY OF ANY
PORTION OF THIS SOFTWARE, THEN THE DISCLAIMER BELOW MUST ALSO BE INCLUDED.
 
THIS SOFTWARE IS PROVIDED AS IS, WITHOUT REPRESENTATION FROM MICHIGAN STATE
UNIVERSITY AS TO ITS FITNESS FOR ANY PURPOSE, AND WITHOUT WARRANTY BY
MICHIGAN STATE UNIVERSITY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING
WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE. THE MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES SHALL
NOT BE LIABLE FOR ANY DAMAGES, INCLUDING SPECIAL, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM ARISING OUT OF OR IN
CONNECTION WITH THE USE OF THE SOFTWARE, EVEN IF IT HAS BEEN OR IS HEREAFTER
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 
Written by Devin Higgins, 2015
(c) Michigan State University Board of Trustees
Licensed under GNU General Public License (GPL) Version 2.
"""

from __future__ import division
import os
import sys
import re
import json
from lxml import etree
from nltkfunctions import NltkText
from citations import ProcessCitations
from plotlygraph import PlotlyGraph
import codecs
from text2num import text2num
import locale
import numpy as np
from pnashtml import PnasHtml

locale.setlocale( locale.LC_ALL, 'en_US.UTF-8' )

class PnasText():
    """Prepare and process full-text downloaded from PNAS."""

    def __init__(self):
        pass

    def load_text(self, file_dir, file_ending):
        """
        Grab all files from specified directory and store in list.

        Positional arguments:
        file_dir (str) -- file directory from which to compile files to analyze.
        file_ending (str) -- gather files only with the specified ending, usually "xml" or "txt".
        """
        self.file_dir = file_dir
        self.file_ending = file_ending
        self.files = (f for f in os.listdir(self.file_dir) if f.lower().endswith(self.file_ending))
        self.quantifiers = {}
        self.quantifiers_data = []

    def clean_text(self, output_dir):
        """
        Remove references and other excess material from individual html-derived text files.

        Positional arguments:
        output_dir (str) -- directory to store cleaned files.
        """
        for index, txt_file in enumerate(self.files):
            if index % 100 == 0:
                print "Cleaned {0} files".format(index)
            new_text = u""
            with codecs.open(os.path.join(self.file_dir, txt_file), "r", "utf-8") as txt_lines:
                for index, line in enumerate(txt_lines):
                    if line.startswith("References"):
                        break

                    else:
                        new_text += line

            with codecs.open(os.path.join(output_dir, txt_file), "w", "utf-8") as output_file:
                output_file.write(new_text)
                print "...written to {0}".format(output_dir)


    def analyze_text(self, word_list, quantifiers_list):
        """
        Start series of file actions to prepare and analyze files.

        Positional arguments:
        word_list (str) -- location of json file containing specific words to seek and record contextual information for.
        quantifiers_list (str) -- location of json file containing 'quantifying' words, to seek associated numbers.
        """
        self.word_list = word_list
        self.quantifiers_list = quantifiers_list
        self.results = []
        for index, txt_file in enumerate(self.files):
            self.txt_file = txt_file
            self.file_location = os.path.join(self.file_dir, self.txt_file)
            self.full_text = codecs.open(self.file_location, "r", "utf-8").read()
            self._get_metadata()
            self._prepare_text()
            print u"{3} {0} {1} {2}".format(self.document_data["year"], self.document_data["title"], self.document_data["doi"], index+1)

            self.document_data["token_contexts"] = self.nltk_text.look_for_words(self.word_list, self.quantifiers_list)
            self.document_data["token_counts"] = self.nltk_text.count_words()
            #self._update_quantifiers(self.nltk_text.find_quantifiers())
            self.results.append(self.document_data)
            if index % 100 == 0:
                sys.stdout.write("Processed {0} articles\r".format(index+1))
                sys.stdout.flush()

        sys.stdout.write("Processing Complete: Analyzed {0} articles".format(len(list(self.files))))

    def match_article_type(self):
        """From all files, find only ones that match a given list of types."""
        match = False
        self.cit.get_article_by_doi(self.doi)
        if self.cit.get_type() in self.document_types:
            match = True

        return match

    def process_refs(self):
        """Open set of references corresponding to article."""
        self.ref_location = self.file_location.replace("txt_ocr", "references").replace("ocr", "references").replace("txt", "xml")
        self.ref_tree = etree.parse(self.ref_location)
        self.ref_root = self.ref_tree.getroot()
        self.ref_text = ""
        for ref in self.ref_root:
            if not ref.text.endswith("."):
                ref_full_text = ref.text + "."
            else:
                ref_full_text = ref.text
            self.ref_text += ref_full_text

        self.nltk_text.reference_text = self.ref_text.replace("- ", "-")


    def find_word_pattern(self):
        """Find matches for a series of patterns."""

        for index, xml_file in enumerate(self.files):
            self.xml_file = xml_file
            self.doi = os.path.splitext(self.xml_file)[0][4:].replace("_", "/")
            self.file_location = os.path.join(self.file_dir, self.xml_file)
            self._parse_xml()

    def load_source_file(self, source="default"):

        self.source = source
        if self._load_data():
            "Data loaded successfully"

        else:
            "Unable to load data"


    def generate_counts_by_year(self, word, graph=False, by_bin=True, visual_output=True, journal="american_naturalist", visual_output_dir="visual_output/[journal]/html"):
        """
        Compile individual article data into yearly counts.

        Positional arguments:
        word (str) -- word must have been analyzed in the 'analyze' function above.

        Keyword arguments:
        graph (bool) -- if true, function returns plotly graph.
        """
        self.word = word
        self.journal = journal
        self.visual_output = visual_output
        self.tokens_by_year = {}
        self.articles_by_year = {}
        self.title_and_doi_by_year = {}
        self.visual_output_dir = visual_output_dir.replace("[journal]", journal)
        if hasattr(self, "data_source"):
            self.results = self.data_source
        
        for article in self.results:
            year = article["year"]
            if word in article["token_counts"]:

                #self.tokens_by_year[year] = self.tokens_by_year.get(year, 0) + article["token_counts"][word]
                if by_bin:
                    if article["token_counts"][word] > 2:
                        self.tokens_by_year[year] = self.tokens_by_year.get(year, 0) + 1
                        if year in self.title_and_doi_by_year:
                            self.title_and_doi_by_year[year].append((article["doi"], article["title"]))
                        else:
                            self.title_and_doi_by_year[year] = [(article["doi"], article["title"])]

                else:
                    self.tokens_by_year[year] = self.tokens_by_year.get(year, 0) + 1
            self.articles_by_year[year] = self.articles_by_year.get(year, 0) + 1

        if graph:
            self.generate_graph(by_bin=by_bin)

    def generate_graph(self, by_bin=True):
        """Generate graph of word frequency over time."""
        ranges = {"ecology": {
                                "xaxis": [1920, 2015],
                                "yaxis": [0, 0.5],
                                },
                  "american_naturalist": {
                                "xaxis": [1880, 2015],
                                "yaxis": [0, 0.5],
                  }
        }
        xdata = []
        ydata = []
        text = []
        print "Generating graph for {0}...".format(self.word)
        for year in sorted(self.tokens_by_year.keys()):
            xdata.append(year)
            ydata.append(self.tokens_by_year[year] / self.articles_by_year[year])
            text.append("{0}: {1} out of {2}".format(year, self.tokens_by_year[year], self.articles_by_year[year]))


        graph = PlotlyGraph()
        graph.BuildGraph(xdata, ydata, "Year", "Articles Per Year", 
                        u"Ratio of articles with 3 or more appearances. <a href='http://devinhiggins.info/pnas/{2}'>View Article Titles</a><br>Journal: <i>{0}</i><br>Token: <b>{1}</b>".format(self.journal.replace("_", "").upper(), self.word, self.word.replace(" ", "_")+".html"), 
                        text=text,
                        find_inflection=True,
                        )

        if self.visual_output:
            self.graph_url = graph.plot_url
            self._build_visual_output()

    def _build_visual_output(self):
        """Create HTML pages for results to allow for navigation by users."""
        index_page = u"<li><a target='_blank' href={0}>{1}</a></li>".format(self.graph_url, self.word)
        with codecs.open(os.path.join(self.visual_output_dir, "results_index.html"), "a", "utf-8") as f:
            f.write(index_page)


        with codecs.open(os.path.join(self.visual_output_dir, self.word.replace(" ", "_")+".html"), "w", "utf-8") as f:
            f.write(u"<h2>{0}</h2>".format(self.word))
            for year in sorted(self.title_and_doi_by_year.keys()):
                f.write(u"<h3>{0}</h3>".format(year))
                f.write("<ul>")
                for record in self.title_and_doi_by_year[year]:
                    f.write(u"<li>{0}, <a target='_blank' href=http://doi.org/{1}>{1}</a></li>".format(record[1],record[0]))
                f.write("</ul>")


    def _parse_xml(self):
        """Use lxml to parse xml file."""
        self.tree = etree.parse(self.file_location)
        self.root = self.tree.getroot()
        self._get_text()


    def _get_text(self):
        """Pull full text from each page."""
        self.full_text = self._clean_page_text()
        if self.store_text:
            if self.remove_refs:
                self.ref_text = self._get_sample_ref()
                if self.ref_text is not None:
                    print self.ref_text
                    if self.ref_text in self.full_text:
                        print "yes"

                    else:
                        print "no"
                else:
                    print "No ref"

            self._write_file()

        if self.process_text:
            self._get_metadata()

    def _get_sample_ref(self):
        """Open set of references corresponding to article."""
        self.ref_location = self.file_location.replace("ocr", "references")
        self.ref_tree = etree.parse(self.ref_location)
        self.ref_root = self.ref_tree.getroot()
        if len(self.ref_root) > 1:
            self.ref_text = self.ref_root[1].text.rstrip().lstrip()
        else:
            self.ref_text = None
        return self.ref_text

    def _write_file(self):

        with codecs.open(os.path.join(self.txt_path, self.xml_file[:-4]+".txt"), "w", "utf-8") as outfile:
            outfile.write(self.full_text)

        "{0} Completed {1}".format(self.index, self.doi)


    def _get_metadata(self):
        """Cross reference text file to html file."""
        pnh = PnasHtml(self.file_location.replace("txt", "html"))
        self.document_data = {
                 "doi": pnh.get_data("citation_doi"),
                 "title": pnh.get_data("citation_title"),
                 "journal": pnh.get_data("citation_journal_title"),
                 "year": pnh.get_data("DC.Date")[:4],
        }

    def _prepare_text(self):
        """Create NltkText class object for analyzing text."""
        #Use regex to replace line-split words such as 'eco- logy' with 'ecology.'
        self.nltk_text = NltkText(re.sub(r'([A-z])(-\s+)', r'\1', self.full_text))

    def _update_quantifiers(self, quantifiers):
        """
        Create frequency distribution of quantifiers.

        Positional arguments:
        quantifiers (list) -- contains tuples of a number element and a following noun.
        """
        for number, word in quantifiers:
            self.quantifiers[word] = self.quantifiers.get(word, 0) + 1

        self.quantifiers_data.append(quantifiers)


    def generate_reports(self, source="default", file_ending="test"):
        """
        Generate output files for assessment and analysis.

        Keyword arguments:
        source (str) -- if "default", attempt to access self.results object. Otherwise must be path to JSON file.
        file_ending (str) -- string to append to all filenames.
        """
        self.source = source
        self.file_ending = file_ending

        if self._load_data():
            self._start_compiler()


    def average_samples(self, source="default", measures=["mean", "trimmed_mean", "count_over_value"], quantifier_for_count="n =", graph_results=True, count_over_value="1000",visual_output=True, journal="american_naturalist", visual_output_dir="visual_output/[journal]/html"):

        self.measures = measures
        self.visual_output = visual_output
        self.count_over_value = count_over_value
        self.graph_results = graph_results
        self.quantifier_for_count = quantifier_for_count
        self.source = source
        self.journal = journal
        self.visual_output_dir = visual_output_dir.replace("[journal]", journal)
        self.count_data_template = {"year": ["int_list_1", "int_list_2", "..."]}
        self.count_data = {}
        self.quant_title_and_doi_by_year = {}

        if self._load_data():
            self._start_counter()

    def _start_counter(self):
        for record in self.data_source:
            year = record["date"][:4]
            if year not in self.count_data:
                self.count_data[year] = []
            counts = [c[0] for c in record["token_contexts"]["quantifiers"][self.quantifier_for_count]]

            int_list = self._derive_ints(counts)
            if len(int_list) > 0:
                if year in self.quant_title_and_doi_by_year:
                    self.quant_title_and_doi_by_year[year].append((record["doi"], record["title"]))
                else:
                    self.quant_title_and_doi_by_year[year] = [(record["doi"], record["title"])]

                self.count_data[year].append(int(np.mean(int_list)))
        """
        for year in sorted(self.count_data.keys()):
            if len(self.count_data[year]) > 0:
                year_average = int(np.mean(self.count_data[year]))
            else:
                year_average = 0
            print year, year_average, len(self.count_data[year])
        """
        if self.graph_results:

            self.graph_links = {}
            for measure in self.measures:
                graph = PlotlyGraph()
                xdata = []
                ydata = []
                graph_text = []
                for year in sorted(self.count_data.keys()):
                    if len(self.count_data[year]) > 0:
                        xdata.append(year)
                        if measure == "trimmed_mean":
                            trimmed_data = self.trim_data(self.count_data[year])
                            ydata.append(np.mean(trimmed_data))
                            graph_text.append("{1}: Based on {0} articles".format(len(self.count_data[year]), year))

                        if measure == "mean":
                            ydata.append(np.mean(self.count_data[year]))
                            graph_text.append("{1}: Based on {0} articles".format(len(self.count_data[year]), year))

                        if measure == "count_over_value":
                            ydata.append(len([x for x in self.count_data[year] if x > self.count_over_value])/len(self.count_data[year]))
                            graph_text.append("{1}: Based on {0} articles".format(len(self.count_data[year]), year))

                    else:
                        xdata.append(year)
                        ydata.append(0)
                        graph_text.append("No data")

                graph.BuildGraph(xdata, ydata, "Year", "Average Number", "{1} for '{0}'".format(self.quantifier_for_count, measure), text=graph_text)
    
                if self.visual_output:
                    self.graph_url = graph.plot_url
                    self.graph_links[measure] = self.graph_url
                    

            self._build_visual_output_quantifier()


    def _build_visual_output_quantifier(self):
        """Create HTML pages for results to allow for navigation by users."""
        index_page = u"<li>{0} ".format(self.quantifier_for_count)
        for measure in self.graph_links:
            index_page += u"<a target='_blank' href={0}>{1}</a> ".format(self.graph_links[measure], measure)
        index_page += u"</li>"

        with codecs.open(os.path.join(self.visual_output_dir, "results_index_test.html"), "a", "utf-8") as f:
            f.write(index_page)


        with codecs.open(os.path.join(self.visual_output_dir, "quantifier"+self.quantifier_for_count.replace(" ", "_")+".html"), "w", "utf-8") as f:
            f.write(u"<h2>{0}</h2>".format(self.quantifier_for_count))
            for year in sorted(self.quant_title_and_doi_by_year.keys()):
                print year, str(len(self.quant_title_and_doi_by_year[year]))
                f.write(u"<h3>{0}</h3>".format(year))
                f.write("<ul>")
                for record in self.quant_title_and_doi_by_year[year]:
                    f.write(u"<li>{0}, <a target='_blank' href=http://ezproxy.msu.edu/login?url=http://www.jstor.org/stable/{1}>{1}</a></li>".format(record[1],record[0]))
                f.write("</ul>")
        #self._run_yearly_averages()

    def trim_data(self, values):
        data = np.array(values)
        return data[abs(data - np.mean(data)) < 3 * np.std(data)]


    def _derive_ints(self, count_list):
        int_counts = []
        for num in count_list:
            try: 
                num_int = int(float(num))

            except:

                try: 
                    num_int = locale.atoi(num)

                except:

                    try:
                        num_int = text2num(num)

                    except:
                        #print "Failed to convert {0}".format(num)
                        num_int = None

            if num_int is not None:
                int_counts.append(num_int)

        return int_counts




    def _load_data(self):
        """Attempt to load data"""
        data_loaded = True

        #Use current object by default
        if self.source == "default" and hasattr(self, results):
            self.data_source = self.results

        #If filepath provided attempt to access data (JSON accepted)
        elif os.path.exists(self.source):
            self.data_source = json.load(open(self.source, "r"))

        else:
            print "No valid data source found."
            data_loaded = False

        return data_loaded

    def _start_compiler(self):
        """Compile data from data source."""

        self._gather_quantifiers_by_category()
        #self._gather_search_words()


    def _gather_search_words(self):

        self.word_list = json.load(codecs.open("word_forms.json", "r", "utf-8"))

        self.search_data_categories = {
                                        "total count":["data"],
                                        "on_off":["information theory","aic","model selection"]
        }

        self.search_tsv_data = ""

        self.search_tsv_data = "title\t"+"doi\t"+"journal\t"+"year\t"+"\t".join(self.word_list.keys())+"\t\n"
        for index,record in enumerate(self.data_source):
            if index%100 == 0:
                print "Processed {0} records".format(index)
            self.search_tsv_data += "\t".join([record["title"], record["doi"], record["journal"], record["date"][:4]]) + "\t"
            for search_term in self.word_list:
                if search_term in record["token_contexts"]:
                    search_data = record["token_contexts"][search_term]
                    if len(search_data) > 1:
                        self.search_tsv_data += unicode(len(search_data)) + u" ⟶ " + " || ".join((item[1]) for item in search_data) + "\t"
                    else:
                        self.search_tsv_data += "\t"
                else:
                    self.search_tsv_data += "\t"

            self.search_tsv_data += "\n"

        self._make_search_tsv()

    def _make_search_tsv(self):

        print "Writing TSV..."

        with codecs.open(("output/search"+"_"+self.file_ending+".tsv").replace(" ", "_"), "w", "utf-8") as outfile:
            outfile.write(self.search_tsv_data)


    def _gather_quantifiers_by_category(self):
        """Use taxonomy provided by ECO team to create TSV output."""
        self.q_categories = {
                        "space":["feet","mm","m","inches","cm","miles","km","fathoms","nm"],
                        "time":["years","days","s","yr","weeks","months","hours","day","year","minutes","d"],
                        "taxa":["species","individuals","genera","orders","families","classes"],
                        "temporal repetition":["times", "generations"],
                        "sample size":["size =", "n =", "x =", "specimens", "df ="],
                        "potential sample size":["lines", "points", "sites", "population", "plant", "cells", "thousand", "traits", "forms", "kinds", "values", "plates", "plants", "eggs", "types", "groups", "pairs", "females", "males", "populations"],
                        "analytical size":["study", "parameters", "factors", "model", "models", "df", "studies"]                
                        }

        self.q_tsv_data = {}

        for cat, columns in self.q_categories.items():
            self.q_tsv_data[cat] = "title\t"+"doi\t"+"journal\t"+"year\t"+"\t".join(columns)+"\t\n"

        self._extract_q_data()

    def _extract_q_data(self):

        for record in self.data_source:
            for cat, columns in self.q_categories.items():
                self.q_tsv_data[cat] += "\t".join([record["title"], record["doi"], record["journal"], record["date"][:4]]) + "\t"
                for column in columns:
                    column_data = record["token_contexts"]["quantifiers"][column]
                    if len(column_data) > 1:
                        self.q_tsv_data[cat] += unicode(len(column_data)) + u"x ⟶ " + " || ".join((item[0] + " : " + item[1]) for item in column_data) + "\t"
                    else:
                        self.q_tsv_data[cat] += "\t"
                self.q_tsv_data[cat] += "\n"

        self._make_tsv("quantifiers")

    def _make_tsv(self, type):

        for dataset in self.q_tsv_data:
            with codecs.open(("output/quantifiers_"+dataset+"_"+self.file_ending+".tsv").replace(" ", "_"), "w", "utf-8") as outfile:
                outfile.write(self.q_tsv_data[dataset])

