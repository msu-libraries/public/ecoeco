#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
COPYRIGHT © 2015 
MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES
ALL RIGHTS RESERVED
 
PERMISSION IS GRANTED TO USE, COPY, CREATE DERIVATIVE WORKS AND REDISTRIBUTE
THIS SOFTWARE AND SUCH DERIVATIVE WORKS FOR ANY PURPOSE, SO LONG AS THE NAME
OF MICHIGAN STATE UNIVERSITY IS NOT USED IN ANY ADVERTISING OR PUBLICITY
PERTAINING TO THE USE OR DISTRIBUTION OF THIS SOFTWARE WITHOUT SPECIFIC,
WRITTEN PRIOR AUTHORIZATION.  IF THE ABOVE COPYRIGHT NOTICE OR ANY OTHER
IDENTIFICATION OF MICHIGAN STATE UNIVERSITY IS INCLUDED IN ANY COPY OF ANY
PORTION OF THIS SOFTWARE, THEN THE DISCLAIMER BELOW MUST ALSO BE INCLUDED.
 
THIS SOFTWARE IS PROVIDED AS IS, WITHOUT REPRESENTATION FROM MICHIGAN STATE
UNIVERSITY AS TO ITS FITNESS FOR ANY PURPOSE, AND WITHOUT WARRANTY BY
MICHIGAN STATE UNIVERSITY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING
WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE. THE MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES SHALL
NOT BE LIABLE FOR ANY DAMAGES, INCLUDING SPECIAL, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM ARISING OUT OF OR IN
CONNECTION WITH THE USE OF THE SOFTWARE, EVEN IF IT HAS BEEN OR IS HEREAFTER
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 
Written by Devin Higgins, 2015
(c) Michigan State University Board of Trustees
Licensed under GNU General Public License (GPL) Version 2.
"""

from bs4 import BeautifulSoup
import codecs

class PnasHtml():
    """Class to parse metadata from PNAS html file."""

    def __init__(self, file_path):
        """
        Open HTML file and prepare for parse.

        Positional arguments:
        file_path (str) -- path to html file to parse
        """
        self.file_path = file_path
        html = codecs.open(self.file_path, "r", "utf-8").read()
        self.__soupify(html)

    def __soupify(self, html):
        """
        Make beautiful soup object from supplied html.

        Positional arguments:
        html (str) -- html data provided in unicode string format
        """
        self.soup = BeautifulSoup(html)

    def get_data(self, element):
        """
        Return piece of data from parsed html.

        Positional arguments:
        element (str) -- the element value for the attribute 'name'. Returns the value of the associated 'content' attribute.
        """
        try:
            tag = self.soup.find("meta", attrs={"name":element})
            return tag["content"]

        except:
            return u"[None]"


