#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
COPYRIGHT © 2015 
MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES
ALL RIGHTS RESERVED
 
PERMISSION IS GRANTED TO USE, COPY, CREATE DERIVATIVE WORKS AND REDISTRIBUTE
THIS SOFTWARE AND SUCH DERIVATIVE WORKS FOR ANY PURPOSE, SO LONG AS THE NAME
OF MICHIGAN STATE UNIVERSITY IS NOT USED IN ANY ADVERTISING OR PUBLICITY
PERTAINING TO THE USE OR DISTRIBUTION OF THIS SOFTWARE WITHOUT SPECIFIC,
WRITTEN PRIOR AUTHORIZATION.  IF THE ABOVE COPYRIGHT NOTICE OR ANY OTHER
IDENTIFICATION OF MICHIGAN STATE UNIVERSITY IS INCLUDED IN ANY COPY OF ANY
PORTION OF THIS SOFTWARE, THEN THE DISCLAIMER BELOW MUST ALSO BE INCLUDED.
 
THIS SOFTWARE IS PROVIDED AS IS, WITHOUT REPRESENTATION FROM MICHIGAN STATE
UNIVERSITY AS TO ITS FITNESS FOR ANY PURPOSE, AND WITHOUT WARRANTY BY
MICHIGAN STATE UNIVERSITY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING
WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE. THE MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES SHALL
NOT BE LIABLE FOR ANY DAMAGES, INCLUDING SPECIAL, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM ARISING OUT OF OR IN
CONNECTION WITH THE USE OF THE SOFTWARE, EVEN IF IT HAS BEEN OR IS HEREAFTER
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 
Written by Devin Higgins
April 1, 2015 - Ongoing
(c) Michigan State University Board of Trustees
Licensed under GNU General Public License (GPL) Version 2.
"""

import nltk
from nltk.corpus import wordnet as wn
import re
import json
import codecs

class NltkText():
    """Perform NLTK operations and other string analysis on text"""

    def __init__(self, text):
        """
        Access text.

        Positional arguments:
        text (str) -- text to process.
        """
        self.text = text
        self.reference_text = ""
        self.sentences = self.sentence_tokenize()
        self.sentences_words = self.word_tokenize()

    def sentence_tokenize(self):
        """
        Tokenize according to specified paradigm.
        TODO: Add support for a variety of tokenization schemes.
        """
        return nltk.sent_tokenize(self.text)

    def word_tokenize(self):
        """
        Tokenize according to specified scheme.
        TODO: Add support for a variety of tokenization schemes.
        """
        self.sentences_words = []
        for sentence in self.sentences:
            self.sentences_words.append([s.lower().strip(" ./,;:'()[]{}") for s in sentence.split()])

        return self.sentences_words

    def look_for_words(self, word_forms, quantifiers_list, check_refs=False, get_quantifiers=True, citation=None):
        """
        Look for each word or bigram in a list of words.

        Positional arguments:
        word_forms (str) -- path to word_forms dictionary containing all forms of word to be searched for.
        quantifiers_list (str) -- path to list of quantifiers

        Keyword arguments:
        check_refs (bool) -- if True compare results of word search with values in references.
        get_quantifiers (bool) -- if True load list of quantifiers and get preceding (or following) numbers.
        """
        self.check_refs = check_refs
        self.word_results = {}
        self.get_quantifiers = get_quantifiers
        self.word_forms = word_forms
        self.quantifiers_list = quantifiers_list
        self._get_word_list()
        for word in self.word_list:
            self.word_results[word] = []

        if get_quantifiers:
            self._get_quantifiers()
            self.word_results["quantifiers"] = {}
            for quant in self.quantifiers:
                self.word_results["quantifiers"][quant] = []

        if citation:
            self.word_results[citation] = []

        for index, sentence in enumerate(self.sentences):

            self.index = index
            self.sentence = sentence
            self.sentence_words = self.sentences_words[index]

            if citation:
                self.word_results[citation] = []
                if all(w in self.sentence_words for w in citation.split()):
                    self.word_results[citation].append((index, sentence))


            for word in self.word_list:

                self.word = word
                self._look_for_word()

            if self.get_quantifiers:

                self.find_quantifiers()

        return self.word_results

    def count_words(self):
        """Calculate number of occurences for each word searched for."""
        self.word_counts = {}
        if not self.word_results:
            print "No results available."
        else:
            for word in self.word_results:
                self.word_counts[word] = len(self.word_results[word])
        return self.word_counts

    def find_quantifiers(self):
        """Looks for instances of digits followed by nouns."""
        self.quantifier_results = []
        pre_quants = ["n", "n =", "mean =", "f =", "p =", "x =", "size =", "sd =", "range =", "df =", "mean", "f", "p", "x", "size", "sd", "range", "df"]
        bigrams = [" ".join([n[0], n[1]]) for n in nltk.bigrams(self.sentence_words)]

        tagged_sentence = nltk.pos_tag(self.sentence_words)
        for quant in self.quantifiers:
            if quant in self.sentence_words and self.sentence not in self.reference_text:
                indices = [i for i,word in enumerate(self.sentence_words) if word == quant]
                for index in indices:  
                    if quant == "exploratory" and index <= len(tagged_sentence) - 2:
                        if "NN" in tagged_sentence[index+1][1]:
                            self.quant = quant
                            self.target_token = tagged_sentence[index+1][0]
                            self._update_quantifier_results()

                    elif tagged_sentence[index-1][1] == "CD" and quant not in pre_quants:
                        self.quant = quant
                        self.target_token = tagged_sentence[index-1][0]
                        self._update_quantifier_results()

                    elif quant in pre_quants and index <= len(tagged_sentence) - 2:
                        if tagged_sentence[index+1][1] == "CD":
                            self.quant = quant
                            self.target_token = tagged_sentence[index+1][0]
                            self._update_quantifier_results()

            elif quant in bigrams and self.sentence not in self.reference_text:
                indices = [i for i,bigram in enumerate(bigrams) if bigram == quant]
                for index in indices:  
                    if index <= len(bigrams) - 2:
                        self.quant = quant
                        if len(bigrams[index+1].split()) > 1:
                            self.target_token = bigrams[index+1].split()[1]
                        else:
                            self.target_token = bigrams[index+1]
                        self._update_quantifier_results()


    def _get_quantifiers(self):
        """Load set of quantifier words."""
        self.quantifiers = json.load(codecs.open(self.quantifiers_list, "r", "utf-8"))

    def _get_word_list(self):
        self.word_list = json.load(codecs.open(self.word_forms, "r", "utf-8"))

    def _look_for_quantifier(self):
        """Look for each word or bigram in a list of words."""

        bigrams = [" ".join([n[0], n[1]]) for n in nltk.bigrams(self.sentence_words)]

        if self.word in self.sentence_words[self.index] and self.sentence not in self.reference_text:
            self._update_quantifier_results()

        elif self.word in bigrams:
            self._update_quantifier_results()


    def _look_for_word(self):
        """Look for each word or bigram in a list of words."""

        bigrams = [" ".join([n[0], n[1]]) for n in nltk.bigrams(self.sentence_words)]

        if any(w in self.sentences_words[self.index] for w in self.word_list[self.word]) and self.sentence not in self.reference_text:
            self._update_results()

        #elif any(w in self.sentences_words[self.index] for w in self.word_list[self.word]):
            #print self.sentence

        elif any(w in bigrams for w in self.word_list[self.word]):
            self._update_results()

        
        """
        if self.word in self.sentence:
            self._update_results()
        """

    def _update_results(self):
        """Add to data dictionary of word results."""
        self.word_results[self.word].append((self.index, self.sentence))


    def _update_quantifier_results(self):
        """Add to data dictionary of word results."""
        self.word_results["quantifiers"][self.quant].append((self.target_token, self.sentence))




