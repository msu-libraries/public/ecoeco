#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
COPYRIGHT © 2015 
MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES
ALL RIGHTS RESERVED
 
PERMISSION IS GRANTED TO USE, COPY, CREATE DERIVATIVE WORKS AND REDISTRIBUTE
THIS SOFTWARE AND SUCH DERIVATIVE WORKS FOR ANY PURPOSE, SO LONG AS THE NAME
OF MICHIGAN STATE UNIVERSITY IS NOT USED IN ANY ADVERTISING OR PUBLICITY
PERTAINING TO THE USE OR DISTRIBUTION OF THIS SOFTWARE WITHOUT SPECIFIC,
WRITTEN PRIOR AUTHORIZATION.  IF THE ABOVE COPYRIGHT NOTICE OR ANY OTHER
IDENTIFICATION OF MICHIGAN STATE UNIVERSITY IS INCLUDED IN ANY COPY OF ANY
PORTION OF THIS SOFTWARE, THEN THE DISCLAIMER BELOW MUST ALSO BE INCLUDED.
 
THIS SOFTWARE IS PROVIDED AS IS, WITHOUT REPRESENTATION FROM MICHIGAN STATE
UNIVERSITY AS TO ITS FITNESS FOR ANY PURPOSE, AND WITHOUT WARRANTY BY
MICHIGAN STATE UNIVERSITY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING
WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE. THE MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES SHALL
NOT BE LIABLE FOR ANY DAMAGES, INCLUDING SPECIAL, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM ARISING OUT OF OR IN
CONNECTION WITH THE USE OF THE SOFTWARE, EVEN IF IT HAS BEEN OR IS HEREAFTER
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 
Originally created by Devin Higgins, 2015
(c) Michigan State University Board of Trustees
Licensed under GNU General Public License (GPL) Version 2.
"""

from lxml import etree
import os

class JstorReference():
    """Process an individual JSTOR reference."""

    def __init__(self, reference):
        """
        Initiate processing of XML files in the supplied directory.

        Positional arguments:
        reference (etree Element) -- lxml-created reference object from parsed XML file.
        """
        self.reference = reference
        self._get_text()

    def _get_text(self):
        """Pull text from Jstor reference."""
        self.text = self.reference.text

    def match_words(self, search_words):
        """
        Check for presence of all words in supplied list in reference text.

        Positional arguments:
        ref_words (list) -- words to check for in references.
        """
        self.search_words = search_words
        self._prepare_text()
        return self.match

    def _prepare_text(self):
        """Split text into punctuation-less words."""
        self.reference_words = [word.lower().rstrip(":;.,/?!][()'<>").lstrip(":;.,/?!][()'<>") for word in self.text.split()]
        self._is_match()

    def _is_match(self):
        """Check if all words in search words appear in reference words."""
        self.match = all(search_word in self.reference_words for search_word in self.search_words)





