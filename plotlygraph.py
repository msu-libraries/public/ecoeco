#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
COPYRIGHT © 2015 
MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES
ALL RIGHTS RESERVED
 
PERMISSION IS GRANTED TO USE, COPY, CREATE DERIVATIVE WORKS AND REDISTRIBUTE
THIS SOFTWARE AND SUCH DERIVATIVE WORKS FOR ANY PURPOSE, SO LONG AS THE NAME
OF MICHIGAN STATE UNIVERSITY IS NOT USED IN ANY ADVERTISING OR PUBLICITY
PERTAINING TO THE USE OR DISTRIBUTION OF THIS SOFTWARE WITHOUT SPECIFIC,
WRITTEN PRIOR AUTHORIZATION.  IF THE ABOVE COPYRIGHT NOTICE OR ANY OTHER
IDENTIFICATION OF MICHIGAN STATE UNIVERSITY IS INCLUDED IN ANY COPY OF ANY
PORTION OF THIS SOFTWARE, THEN THE DISCLAIMER BELOW MUST ALSO BE INCLUDED.
 
THIS SOFTWARE IS PROVIDED AS IS, WITHOUT REPRESENTATION FROM MICHIGAN STATE
UNIVERSITY AS TO ITS FITNESS FOR ANY PURPOSE, AND WITHOUT WARRANTY BY
MICHIGAN STATE UNIVERSITY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING
WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE. THE MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES SHALL
NOT BE LIABLE FOR ANY DAMAGES, INCLUDING SPECIAL, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM ARISING OUT OF OR IN
CONNECTION WITH THE USE OF THE SOFTWARE, EVEN IF IT HAS BEEN OR IS HEREAFTER
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 
Module originally created by Devin Higgins
April 1, 2015 - Ongoing
(c) Michigan State University Board of Trustees
Licensed under GNU General Public License (GPL) Version 2.
"""

import ConfigParser
import plotly.plotly as py
from plotly.graph_objs import *
import os
import numpy as np

class PlotlyGraph():

    def __init__(self):
        """Establish plotly log-in."""

        try:
            self._plotly_sign_in()
        except Exception as e:
            print "Sign-in to Plotly failed:", e


    def _plotly_sign_in(self):
        """Sign in to plotly with configs retrieved from config file."""
        cwd = os.path.dirname(os.path.abspath(__file__))
        config_file = "config/default.cfg"
        config = ConfigParser.ConfigParser()
        config.read(os.path.join(cwd, config_file))
        username = config.get("plotly", "username")
        key = config.get("plotly", "key")
        py.sign_in(username, key)


    def BuildGraph(self, xdata, ydata, xaxis, yaxis, graph_name, text=None, find_inflection=False):
        """Builds graph using Plotly."""
        self.xdata = [int(x) for x in xdata]
        self.ydata = [round(y*100, 2) for y in ydata]



        layout = Layout(
            title = graph_name,
            xaxis = XAxis(title=xaxis),
            yaxis = YAxis(title=yaxis),
        )

        freq_per_article = Bar(
                                x = self.xdata,
                                y = self.ydata,
                                text=text,
                                name="Ratio of Articles (with 3+ occurrences)",
                                marker = Marker(
                                    color="#0000FF",
                                    line = Line(
                                       color = 'rgba(55, 128, 191, 1.0)',
                                        width = 1,
                                        )
                                    )

                                )

        if find_inflection and len(xdata) > 5:
            self._find_inflection_points()
            inflection_points = Scatter(
                                        x = self.inflection_points_x,
                                        y = self.inflection_points_y,
                                        mode = "markers",
                                        name = "Inflection Point(s): {0}".format(",".join([str(int(x)) for x in self.inflection_points_x])),
                                            )
                                        

            best_fit_line = Scatter(
                                    x = xdata,
                                    y = [self.polynomial(int(year)) for year in xdata],
                                    mode="lines",
                                    connectgaps=True,
                                    name="Best Fit Line",
                                    )

            data = Data([freq_per_article, best_fit_line, inflection_points])

        else:

            data = Data([freq_per_article])

        figure = Figure(data=data, layout=layout)
        self.plot_url = py.plot(figure, filename=graph_name, auto_open=False)


    def _find_inflection_points(self):
        """Find points of zero concavity in graph data."""
        self.inflection_points_x = []
        self.inflection_points_y = []
        x = [float(x) for x in self.xdata]
        y = [float(y) for y in self.ydata]
        self.best_fit = np.polyfit(x, y, 4)
        self._take_second_derivative()

    def _take_second_derivative(self):
        """Find second derivative of polynomial best-fit function."""
        self.polynomial = np.poly1d(self.best_fit)
        self.second_deriv = self.polynomial.deriv().deriv()
        self._get_inflection_coordinates()

    def _get_inflection_coordinates(self):
        """Use the second derivative to calculate roots, re-run these values into the original best-fit polynomial."""
        self.roots = [int(root) for root in np.roots(self.second_deriv)]
        for root in self.roots:
            if root in self.xdata:
                self.inflection_points_x.append(root)
                self.inflection_points_y.append(self.polynomial(root))

