#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
COPYRIGHT © 2015
MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES
ALL RIGHTS RESERVED
 
PERMISSION IS GRANTED TO USE, COPY, CREATE DERIVATIVE WORKS AND REDISTRIBUTE
THIS SOFTWARE AND SUCH DERIVATIVE WORKS FOR ANY PURPOSE, SO LONG AS THE NAME
OF MICHIGAN STATE UNIVERSITY IS NOT USED IN ANY ADVERTISING OR PUBLICITY
PERTAINING TO THE USE OR DISTRIBUTION OF THIS SOFTWARE WITHOUT SPECIFIC,
WRITTEN PRIOR AUTHORIZATION.  IF THE ABOVE COPYRIGHT NOTICE OR ANY OTHER
IDENTIFICATION OF MICHIGAN STATE UNIVERSITY IS INCLUDED IN ANY COPY OF ANY
PORTION OF THIS SOFTWARE, THEN THE DISCLAIMER BELOW MUST ALSO BE INCLUDED.
 
THIS SOFTWARE IS PROVIDED AS IS, WITHOUT REPRESENTATION FROM MICHIGAN STATE
UNIVERSITY AS TO ITS FITNESS FOR ANY PURPOSE, AND WITHOUT WARRANTY BY
MICHIGAN STATE UNIVERSITY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING
WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE. THE MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES SHALL
NOT BE LIABLE FOR ANY DAMAGES, INCLUDING SPECIAL, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM ARISING OUT OF OR IN
CONNECTION WITH THE USE OF THE SOFTWARE, EVEN IF IT HAS BEEN OR IS HEREAFTER
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 
Code written by Devin Higgins
April 1, 2015 - Ongoing
(c) Michigan State University Board of Trustees
Licensed under GNU General Public License (GPL) Version 2.
"""

from citations import ProcessCitations
from nltk_functions import NltkText
import sys

class EcoCitations():
    """Prepare and analyze ecology texts with NLTK."""

    def __init__(self, file_path):
        """
        Establish file for processing.

        Postional arguments:
        file_path (str) -- absolute path to citations xml file
        """
        self.results = []
        # Template for self.results
        data_template = [{
                         
                          "title":"",
                          "author":"",
                          "abstract":"",
                          "journal":"",
                          "pubdate":"",
                          "words": {
                                    "word": {
                                             "contexts":[],
                                            }, 
                                    }, 
                        },]

        self.file_path = file_path
        self.count = 0
        self.cit = ProcessCitations(self.file_path)

    def analyze_citation(self, word_list):
        """
        Iterate through citations and analyze each abstract.

        word_list (list) -- list of words to search for in a given text.
        """
        self.word_list = word_list
        for article in self.cit.root:
            self.cit.article_handler(article)
            self.abstract_text = self.cit.get_abstract()
            self._check_abstract()
            sys.stdout.write("Processed {0} articles\r".format(self.count))
            sys.stdout.flush()

        sys.stdout.write("Processing Complete: Analyzed {0} articles".format(self.count))

    def convert_to_json(self):
        """Convert XML citations file to JSON."""
        self.citations_dict = {}
        for article in self.cit.root:
            self.cit.article_handler(article)
            doi = self.cit.get_doi()
            self.citations_dict[doi] = {}
            for element in article:
                self.citations_dict[doi][element.tag] = element.text


    def count_article_types_by_year(self):

        self.article_type_counts = {}
        for article in self.cit.root:
            self.cit.article = article
            year = self.cit.get_date()[:4]
            article_type = self.cit.get_type()
            if year not in self.article_type_counts:
                self.article_type_counts[year] = {}

            self.article_type_counts[year][article_type] = self.article_type_counts[year].get(article_type, 0) + 1

    def output_csv(self):

        csv_data = ""
        csv_data = ",".join(["year", "fla", "mis", "nws", "brv", "edi"]) + "\n"
        for year, data in self.article_type_counts.items():
            csv_data += ",".join([year, str(data.get("fla", 0)), str(data.get("mis", 0)), str(data.get("nws",0)), str(data.get("brv",0)), str(data.get("edi",0))]) + "\n"

        with open("article_types.csv", "w") as outfile:
            outfile.write(csv_data)

    def _check_abstract(self):
        """Check for presence of text in abstract."""
        if self.abstract_text:
            self.count += 1
            self._initialize_results_dict()
        else:
            pass

    def _initialize_results_dict(self):
        """Get metadata and start dictionary for results of analysis."""

        self.doi = self.cit.get_doi()
        self.document_data = {
                         "doi": self.doi,
                         "title": self.cit.get_title(),
                         "author": self.cit.get_author(),
                         "abstract": self.cit.get_abstract(),
                         "journal": self.cit.get_journal(),
                         "date": self.cit.get_date(),
        }

        self._tokenize_text()


    def _tokenize_text(self):
        """Load text for processing."""
        self.nltk_data = Nltk(self.abstract_text)
        self.sentences = self.nltk_data.sentence_tokenize()
        self.words = self.nltk_data.word_tokenize()
        self._gather_words()

    def _gather_words(self):
        """Take list and find results for each word in each supplied text."""
        self.nltk_data.look_for_words(self.word_list)
        self.document_data["words"] = self.nltk_data.results
        self.results.append(self.document_data)



