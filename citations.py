#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
COPYRIGHT © 2015
MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES
ALL RIGHTS RESERVED
 
PERMISSION IS GRANTED TO USE, COPY, CREATE DERIVATIVE WORKS AND REDISTRIBUTE
THIS SOFTWARE AND SUCH DERIVATIVE WORKS FOR ANY PURPOSE, SO LONG AS THE NAME
OF MICHIGAN STATE UNIVERSITY IS NOT USED IN ANY ADVERTISING OR PUBLICITY
PERTAINING TO THE USE OR DISTRIBUTION OF THIS SOFTWARE WITHOUT SPECIFIC,
WRITTEN PRIOR AUTHORIZATION.  IF THE ABOVE COPYRIGHT NOTICE OR ANY OTHER
IDENTIFICATION OF MICHIGAN STATE UNIVERSITY IS INCLUDED IN ANY COPY OF ANY
PORTION OF THIS SOFTWARE, THEN THE DISCLAIMER BELOW MUST ALSO BE INCLUDED.
 
THIS SOFTWARE IS PROVIDED AS IS, WITHOUT REPRESENTATION FROM MICHIGAN STATE
UNIVERSITY AS TO ITS FITNESS FOR ANY PURPOSE, AND WITHOUT WARRANTY BY
MICHIGAN STATE UNIVERSITY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING
WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE. THE MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES SHALL
NOT BE LIABLE FOR ANY DAMAGES, INCLUDING SPECIAL, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM ARISING OUT OF OR IN
CONNECTION WITH THE USE OF THE SOFTWARE, EVEN IF IT HAS BEEN OR IS HEREAFTER
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 
Written by Devin Higgins
2015
(c) Michigan State University Board of Trustees
Licensed under GNU General Public License (GPL) Version 2.
"""

from lxml import etree

class ProcessCitations():
    """Process XML file of citations from JSTOR DFR"""

    def __init__(self, file_path):
        """
        Prepare XML file for processing.

        Postional arguments:
        file_path (str) -- absolute path to citations.xml file
        """
        self.file_path = file_path
        self.tree = etree.parse(file_path)
        self.root = self.tree.getroot()

    def store_citation_data(self, output_dir):
        """
        Read through citations file and store each record as an individual file.

        Positional arguments:
        output_dir (str) -- directory to store newly created xml files.
        """
        for article in self.root:
            self.article = article


    def get_article_by_doi(self, doi):
        """
        Use DOI to look up metadata for item.
        
        Positional arguments:
        doi (str) -- unique identifier found in filename of article ocr, id attribute in citations.xml.
        """
        doi_xpath = "/citations/article[@id='doi']".replace("doi", doi)
        self.articles = self.tree.xpath(doi_xpath)
        if self.articles:
            self.article = self.articles[0]
        else:
            print doi, "Not Found"
            self.article = None

        if len(self.articles) > 1:
            print "Error: More than one article matches supplied DOI"

    def article_handler(self, article):
        """Store lxml etree document for article in class."""
        self.article = article

    def get_abstract(self):
        """Get the abstract for a given article"""
        return self.article.find("abstract").text

    def get_doi(self):
        """Get the doi for a given article"""
        return self.article.find("doi").text

    def get_title(self):
        """Get the title for a given article"""
        return self.article.find("title").text

    def get_author(self):
        authors =  self.article.findall("author")
        return "|".join([a.text for a in authors if a.text is not None])

    def get_date(self):
        return self.article.find("pubdate").text

    def get_journal(self):
        return self.article.find("journaltitle").text

    def get_type(self):
        return self.article.find("type").text
