
Ecology Data Mining
==========

Text Mining Journal Articles
----------------

The `ecoeco` code can be used to run several types of text mining operations on `JSTOR`-provided full text and metadata as well as text seb-scraped from the *Proceedings of the National Academy of Sciences* ([PNAS](http://www.pnas.org/)). Descriptive code is provided below. After cloning the repository, run `python` from the top layer directory, and follow steps below.  

The two main forms of analysis are _word and phrase tallying_ and _quantifier search_.  

Word and phrase tallying draws from a list of words user-generated words and counts mentions of each occurence, recording its sentence-level context. The list of words to be searched is stored in a `JSON` file with this structure, called `word_forms.json`.


    word_forms = {
                  "computation":[
                                "computation",
                                "computational",
                                "computationally",
                                ]
                }

The `key` in the Python dictionary object should be the basic form of the word or phrase to be searched. The `value` should be a list of word forms to search, _including the basic form_. 

Quantifier search attempts to find words that in some way _quantify_ some aspect of the research methodology. These quantifying words are included in `quantifiers.json` as a simple Python list. The code will look for instances in each text where a given quantifier is preceded by a numerical measure, e.g. with numbers or ordinals such as 2, 56, 135, two, ten, etc.

PNAS Content
-----------

*PNAS* content was originally acquired in `HTML` format. The operations below assume that data has been transformed into plain text, then cleaned according to the following method.

Use `clean_text` method to remove references from the end of all files, then save them to a new directory.


    from pnastext import PnasText
    pn = PnasText()
    pn.load_text("/Users/devinhiggins/Dropbox/pnas_text", ".txt")
    pn.clean_text("/Users/devinhiggins/Dropbox/pnas_text_clean")

Use `PnasHtml` class to parse and access data points from the `meta` tags in each HTML file.


    from pnashtml import PnasHtml
    pnh = PnasHtml("/Users/higgi135/Data/ecology/pnas/pnas_html/A_complete_Holocene_record_of_trematode–bivalve_infection_and_implications_for_the_response_of_paras.html")
    print pnh.get_data("citation_title")
    print pnh.get_data("citation_doi")
    print pnh.get_data("citation_journal_title")
    print pnh.get_data("DC.Date")[:4]

The following code can be used to run basic analysis of all text files in the supplied directory. See above for descriptions of `JSON` files that provide terms to analyze.


    from pnastext import PnasText
    pn = PnasText()
    pn.load_text("/Users/devinhiggins/Data/ecology_journals/pnas/pnas_txt", ".txt")
    pn.analyze_text("files/word_forms.json", "files/quantifiers.json")

Save `results` object as JSON file.


    import json
    with open("output/pnas/pnas_results_20150710.json", "w") as f:
        json.dump(pn.results, f)

Code is also included to gather article-level results into year-level reports, which then can be represented visually.


    from pnastext import PnasText
    import json
    pn = PnasText()
    pn.load_source_file(source="output/pnas/")
    for token in sorted(json.load(open('files/word_forms.json', 'r')).keys()):
        pn.generate_counts_by_year(token, graph=True, visual_output=True, journal="PNAS")

Content from Ecology or American Naturalist
-------------------------

Run initial analysis to produce `js.results` object containing counts and contexts for all queried words and quantifiers; Save results as JSON file. 


    from dfrtext import JstorText
    js = JstorText()
    js.load_text("/Users/higgi135/Data/ecology/ecology/txt_ocr","/Users/higgi135/Data/ecology/ecology/citations.xml", ".txt")
    js.analyze_text("files/added_word_forms.json", "files/quantifiers.json")
    import json
    with open("output/ecology/ecology_added_results_20150820.json", "w") as f:
        json.dump(js.results, f)


    from dfrtext import JstorText
    js = JstorText()
    js.load_text("/Users/higgi135/Data/ecology/ecology/ocr", "/Users/higgi135/Data/ecology/ecology/citations.xml", ".xml")
    js.store_text("/Users/higgi135/Data/ecology/ecology/txt_ocr")


    from jstorcalls import JstorCalls
    jsc = JstorCalls()
    jsc.find_reference_matches(["burnham", "anderson", "2002"], "/Users/devinhiggins/Data/ecology_journal_data_20150402/ecology/references", "ecology/citations.json")
    jsc.tally_by_year


    from eco_nltk import EcoCitations
    eco = EcoCitations("american_naturalist/citations.xml")
    eco.convert_to_json()


    from dfrtext import JstorText
    jt = JstorText()
    import json
    jt.load_source_file("output/ecology/ecology_added_results_20150820.json")
    for token in sorted(json.load(open('files/added_word_forms.json', 'r')).keys()):
        jt.generate_counts_by_year(token, graph=True, visual_output=True, journal="ecology")



    len(jsc.tally_by_year["2013"]["titles"])
