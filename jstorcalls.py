#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
COPYRIGHT © 2015 
MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES
ALL RIGHTS RESERVED
 
PERMISSION IS GRANTED TO USE, COPY, CREATE DERIVATIVE WORKS AND REDISTRIBUTE
THIS SOFTWARE AND SUCH DERIVATIVE WORKS FOR ANY PURPOSE, SO LONG AS THE NAME
OF MICHIGAN STATE UNIVERSITY IS NOT USED IN ANY ADVERTISING OR PUBLICITY
PERTAINING TO THE USE OR DISTRIBUTION OF THIS SOFTWARE WITHOUT SPECIFIC,
WRITTEN PRIOR AUTHORIZATION.  IF THE ABOVE COPYRIGHT NOTICE OR ANY OTHER
IDENTIFICATION OF MICHIGAN STATE UNIVERSITY IS INCLUDED IN ANY COPY OF ANY
PORTION OF THIS SOFTWARE, THEN THE DISCLAIMER BELOW MUST ALSO BE INCLUDED.
 
THIS SOFTWARE IS PROVIDED AS IS, WITHOUT REPRESENTATION FROM MICHIGAN STATE
UNIVERSITY AS TO ITS FITNESS FOR ANY PURPOSE, AND WITHOUT WARRANTY BY
MICHIGAN STATE UNIVERSITY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING
WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE. THE MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES SHALL
NOT BE LIABLE FOR ANY DAMAGES, INCLUDING SPECIAL, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM ARISING OUT OF OR IN
CONNECTION WITH THE USE OF THE SOFTWARE, EVEN IF IT HAS BEEN OR IS HEREAFTER
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 
Originally created by Devin Higgins, 2015
(c) Michigan State University Board of Trustees
Licensed under GNU General Public License (GPL) Version 2.
"""
from __future__ import division
from lxml import etree
import os
import json
from jstor_refs import JstorReference
from citations import ProcessCitations
from plotly_graph import PlotlyGraph
from pprint import pprint

class JstorCalls():
    """Make calls to low-level functions and handle data."""

    def __init__(self):
        pass

    def find_reference_matches(self, match_words, directory, metadata_file, run_graph=True):
        """
        Search Jstor references for given list of words.

        Positional arguments:
        ref_words (list) -- list of words to check references for.
        directory (str) -- path from which to pull Jstor reference files
        """
        self.tally_by_year = {}
        self._total_tally = 0
        self.directory = directory
        self.match_words = match_words
        self.metadata_file = metadata_file
        self.reference_files = (os.path.join(directory, ref_file) for ref_file in os.listdir(self.directory) if ref_file.endswith(".XML"))
        self._load_metadata()
        self._iterate_files()
        if run_graph:
            self._run_graph()

    def _run_graph(self):
        """Take computed data and build graph."""
        ply = PlotlyGraph()
        xdata = []
        ydata = []
        text = []
        for key in sorted(self.tally_by_year.keys()):
            if int(key) > 2000:
                xdata.append(key)
                ydata.append(self.tally_by_year[key]["count"]/self.tally_by_year[key]["total"])
                text.append("{0}: {1} out of {2}".format(key, self.tally_by_year[key]["count"], self.tally_by_year[key]["total"]))

        ply.BuildGraph(xdata, ydata, "Year", "Count / Total Articles", "Articles citing {0}".format(" ".join(self.match_words)), text=text)


    def _iterate_files(self):
        """Iterate over reference files and parse references."""
        for index, ref_file in enumerate(self.reference_files):
            self.match = False
            if index % 100 == 0:
                print "Processed {0} files, Found {1} matches".format(index, self._total_tally)
            self.current_file = ref_file
            self._parse_file()

            if self.match:
                self._update_results(1,1)

            if not self.match and self.research_article:
                self._update_results(0,1, add_title=False)



    def _parse_file(self):
        """Parse current file."""
        self._get_citation_metadata()
        if self.current_metadata["type"] == "fla":
            self.research_article = True
            xml = etree.parse(self.current_file)
            self.xml_root = xml.getroot()
            self._parse_references()
        else:
            self.research_article = False

    def _get_citation_metadata(self):
        """Load metadata for current item."""
        self.doi = os.path.splitext(os.path.basename(self.current_file))[0][11:].replace("_", "/")
        self.current_metadata = self.citations_metadata[self.doi] 


    def _load_metadata(self):
        """Open JSON metadata file."""
        self.citations_metadata = json.load(open(self.metadata_file, "r"))


    def _parse_references(self):
        """Parse reference content from reference files."""
        for reference in self.xml_root:
            self.jstor_reference = JstorReference(reference)
            self._find_matches()

    def _find_matches(self):
        """Call method of jstor_reference instance to check for match."""
        if self.jstor_reference.match_words(self.match_words):
            self.match = True
            self._total_tally += 1


    def _update_results(self, count, total, add_title=True):
        """Update results count based on match or not."""
        article_year = self.current_metadata["pubdate"][:4]
        if article_year in self.tally_by_year:
            self.tally_by_year[article_year]["count"] += count
            self.tally_by_year[article_year]["total"] += total
            if add_title:
                self.tally_by_year[article_year]["titles"].append(self.current_metadata["title"])

        else:
            self.tally_by_year[article_year] = {}
            self.tally_by_year[article_year]["count"] = count
            self.tally_by_year[article_year]["total"] = total
            if add_title:
                self.tally_by_year[article_year]["titles"] = [self.current_metadata["title"]]

            else:
                self.tally_by_year[article_year]["titles"] = []



